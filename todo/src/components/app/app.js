import React, { Component } from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from '../item-add-form';

import './app.css';

export default class App extends Component {
    maxId = 1;
    state = {
        todoData: [
            this.createTodoItem('drink'),
            this.createTodoItem('make app'),
            this.createTodoItem('LOL'),
        ],
        status: 'all',
    };
    deleteItem = (id) => {
        this.setState(({todoData}) => {
            const newData = todoData.reduce((todoData, item) => {
                if (item.id === id) {
                    return [...todoData];
                }
                return  [...todoData, item];
            }, []);
            return {
                todoData: newData,
            }
        })
    };

    createTodoItem(label) {
        return {
            label,
            id: this.maxId++,
            important: false,
            done: false,
            visible: true,
        }
    }
    addItem = (label) => {
        this.setState(({todoData}) => {
            return {
                todoData: [
                    ...todoData,
                    this.createTodoItem(label)
                ]
            }
        })
    }

    toggleProperty(arr, id, propName) {
           return arr.map((item) => {
                if (item.id === id) {
                    return {
                        ...item,
                        [propName]: !item[propName],
                    }
                }
                return  item;
            })
    }

    onToggleDone = (id) => {
        this.setState(({todoData}) =>{
           return {
               todoData: this.toggleProperty(todoData, id, 'done'),
           }
        });
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) =>{
            return {
                todoData: this.toggleProperty(todoData, id, 'important'),
            }
        });
    }

    setVisible = (arr, value) => {
        return arr.map( item => {
            return {
                ...item,
                visible: value,
            }
        })
    };

    onSearchItem = (text) => {
        if (text.length === 0) {
            this.setState({
                todoData: this.setVisible(this.state.todoData, true),
            })
        } else {
            this.setState({
                todoData: this.state.todoData.map((item) => {
                    if (item.label.includes(text)) {
                        return item;
                    }
                    return {
                        ...item,
                        visible: false,
                    }
                }),
            })
        }

    };

    changeStatus = (text) => {
        this.setState({
            status: text,
        })
    }

    render() {
        const {todoData} = this.state;
        const doneCount = todoData.filter((item) => item.done).length;
        const todoCount = todoData.length - doneCount;
        const visibleData = todoData.filter(({visible}) => visible);
        const currentData = visibleData.filter(({done}) => {
            if (this.state.status === 'done') {
                return done;
            }
            if (this.state.status === 'active') {
                return !done;
            }
            return true;
        });
        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount} />
                <div className="top-panel d-flex">
                    <SearchPanel onSearch={this.onSearchItem}/>
                    <ItemStatusFilter onChangeStatus={this.changeStatus}
                                      status={this.state.status}
                    />
                </div>

                <TodoList
                    todos={currentData}
                    onDeleted = {this.deleteItem}
                    onToggleDone = {this.onToggleDone}
                    onToggleImportant = {this.onToggleImportant}
                />
                <ItemAddForm
                    onAdd = {this.addItem}
                />
            </div>
        );
    }
};


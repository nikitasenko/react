import React, { Component } from 'react';

import './item-status-filter.css';

export default class ItemStatusFilter extends Component {

    buttons = [
        {name: 'all', label: 'All'},
        {name: 'active', label: 'Active'},
        {name: 'done', label: 'Done'},
    ]

    render() {

        const buttons = this.buttons.map(({label, name}) => {
            const isActive = this.props.status === name;
            return (
                <button type="button"
                        className={isActive ? 'btn btn-info': 'btn btn-outline-secondary'}
                        onClick={() => this.props.onChangeStatus(name)}
                        key={name}
                >{label}</button>
            )
        })

        return (
            <div className="btn-group">
                {buttons}
            </div>
        );
    };
}
